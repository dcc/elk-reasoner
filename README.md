# elk-reasoner

This repository contains the ELK Reasoner 0.4.3 in its binary form. It can be originally downloaded from: https://github.com/liveontologies/elk-reasoner

For more information read the original README.txt 

## License

Licensed under the Apache License, Version 2.0.